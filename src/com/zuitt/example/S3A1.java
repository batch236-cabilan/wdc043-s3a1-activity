package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){

        System.out.println("Input an integer whose factorial will be computed.");

        Scanner userInput = new Scanner(System.in);
        int in = userInput.nextInt();

        try{
            if (in > 0) {
                int i = 1;
                int num =1;

                while(i <= in){
                    num = num * i;
                    //System.out.println(num);
                    i++;
                }
                System.out.println("The factorial of " +in+ " is " +num);
            }else{
                System.out.println("Invalid! Input must be greater than Zero");
            }
        }

        catch(Exception e){
            System.out.println("Invalid Input!");
            // prints the throwable error along with other details like the line number and class name where the exception occurred.
            e.printStackTrace();
        }
    }
}
