package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    //[SECTION] Loops
        // are control structures that allow code blocks to be executed multiple times.
    public static void main(String[] args){
        //while loop
        int x = 0;
        while(x < 10){
            System.out.println("while loop: " +x);
            x++;
        }

        //do-while loop
        int y = 10;
        do{
            System.out.println("Countdown: " +y);
            y--;
        } while(y > 10);

        //for loop
        for(int i = 0; i < 10; i++){
            System.out.println("Current Count: " +i);
        }

        // For loop with array (1st)
        int[] intArray = {100, 200, 300, 400, 500};
        for(int i = 0; i < intArray.length; i++){
           System.out.println(intArray[i]);
       }

        // For loop with array (2nd)
        /* Syntax:
        *   for(dataType itemName(singular) : arrayName(plural)){
        *       //code block
        *   }
        */

        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        //This get each element of the array and assigns it to the variable called name.
        for(String name : nameArray){
            System.out.println(name);
        }

        // Nested loops with multidimensional arrays
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //outer loop that will loop through the rows.
        for(int row = 0; row < 3; row++){
            //inner loop that will through the columns of each row.
            for (int col=0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        //for-each with multidimensional array
        for (String[] row: classroom){
            for(String column: row){
                System.out.println(column);
            }
        }

        //forEach with ArrayList
        /* Syntax:
        *   arrayListName.forEach(Consumer<E> -> //code block);
        *   "->" This is called lambda operator which is used to separate the parameter and implementation
        *
        */
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);

        numbers.forEach(num -> System.out.println("ArrayList of Numbers: " + num));

        // forEach with HashMaps
        // Syntax:
        // hashMapName.forEach((key, value) -> // code block);
        HashMap<String, Integer> grades = new HashMap<String, Integer>(){{
            put("English", 90);
            put("Math", 95);
            put("Science", 97);
            put("History", 94);
        }};

        System.out.println(grades);

        grades.forEach((subject, grade)->System.out.println("The student grade for "+subject+ " is "+grade));

    }
}
